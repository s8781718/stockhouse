# StockHouse

## Chrome 擴充套件 Tampermonkey 安裝、使用
* 安裝
  * 開啟網頁 [Tampermonkey
](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)，點安裝。
* 使用
  * 新增腳本，將程式貼上儲存即可。  
  ![image.png](./image.png)

## Tampermonkey 腳本說明
* 已委託代領查詢頁全部展開、收合按鈕
  * 在"已委託代領查詢頁"右上角新增一個按鈕，點擊可以切換"全部展開、收合"。


